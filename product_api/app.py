from flask import Flask
from flask_restful import Resource, Api

# initiating instance of Flask app
app = Flask(__name__)
print(type(app))
# passing the entire flask app into initiating an API able object
api = Api(app)

class Product(Resource):
    def get(self):
        return {
            'products': ['Orange Muffin', 'Plain old Muffin', 'Blueberry Muffin'] 
        }


class Productprice(Resource):
    def get(self):
        return {
            'products': {'Orange Muffin': 10,
            'Plain old Muffin': 17,
            'Blueberry Muffin': 12}
        }

api.add_resource(Productprice, '/price')

api.add_resource(Product, '/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)